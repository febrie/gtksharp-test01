﻿using System;
using Gtk;

namespace gtksharp_test01
{
    class Program
    {
        static void Main(string[] args)
        {
            //应用初始化
            Application.Init();
            /**********************************************
             * 创建窗体 form1                            
             * 参数：                                    
             *      标题：Main Application               
             *      大小：400x300 固定大小                
             *      对齐：居中                             
             *********************************************/
            var form1 = new Window("Main Application");
            form1.SetDefaultSize(400, 300);
            form1.WindowPosition = WindowPosition.Center;
            form1.Resizable = false;
            //窗体删除事件
            form1.DeleteEvent += (s, e) =>
            {
                Application.Quit();//主窗体关闭，程序结束
            };
            //窗体加载
            windowInit(form1);
            
            form1.ShowAll();
            //应用运行
            Application.Run();
        }
        //方法：windowInit(Window window) 窗体初始化
        private static void windowInit(Window window)
        {
            //外框
            var hBox_outside = new HBox();
            window.Add(hBox_outside);
            //分为左右布局
            var hBox_insideleft = new HBox();
            var vBox_insideright = new VBox();
            hBox_insideleft.SetSizeRequest(300,300);
            vBox_insideright.SetSizeRequest(100,300);
            hBox_outside.Add(hBox_insideleft);
            hBox_outside.Add(vBox_insideright);
            //右侧添加按键
            var btn_right0 = new Button($"CH");
            btn_right0.SetSizeRequest(100, 50);
            vBox_insideright.PackStart(btn_right0, false, false, 5);
            var btn_right1 = new Button($"FUNC");
            btn_right1.SetSizeRequest(100, 50);
            vBox_insideright.PackStart(btn_right1, false, false, 5);
            var btn_right2 = new Button($"+/˄");
            btn_right2.SetSizeRequest(100, 50);
            vBox_insideright.PackStart(btn_right2, false, false, 5);
            var btn_right3 = new Button($"-/˅");
            btn_right3.SetSizeRequest(100, 50);
            vBox_insideright.PackStart(btn_right3, false, false, 5);
            
            //左侧内部左右布局
            var vBox_insideleft_left = new VBox();
            var vBox_insideleft_right = new VBox();
            hBox_insideleft.Add(vBox_insideleft_left);
            hBox_insideleft.Add(vBox_insideleft_right);
            //添加文本
            var label_insideleft_left0 = new Label($"CH0 ON");
            //var label_insideleft_left1 = new Label($"SQUR");
            var text_insideleft_left1 = new TextView();
            var label_insideleft_left2 = new Label($"50.000 MHz");
            var label_insideleft_left3 = new Label($"3.000 Vpp");
            var label_insideleft_left4 = new Label($"0.0 Deg");
            var label_insideleft_right0 = new Label($"CH1 ON");
            var label_insideleft_right1 = new Label($"SINE");
            var label_insideleft_right2 = new Label($"10.000 MHz");
            var label_insideleft_right3 = new Label($"2.000 Vpp");
            var label_insideleft_right4 = new Label($"30.0 Deg");
            label_insideleft_left0.SetSizeRequest(150, 60);
            //label_insideleft_left1.SetSizeRequest(150, 60);
            text_insideleft_left1.SetSizeRequest(150, 60);
            text_insideleft_left1.AcceptsTab = false;
            var context_text_insideleft_left1 = text_insideleft_left1.Buffer;

            label_insideleft_left2.SetSizeRequest(150, 60);
            label_insideleft_left3.SetSizeRequest(150, 60);
            label_insideleft_left4.SetSizeRequest(150, 60);
            label_insideleft_right0.SetSizeRequest(150, 60);
            label_insideleft_right1.SetSizeRequest(150, 60);
            label_insideleft_right2.SetSizeRequest(150, 60);
            label_insideleft_right3.SetSizeRequest(150, 60);
            label_insideleft_right4.SetSizeRequest(150, 60);
            vBox_insideleft_left.Add(label_insideleft_left0);
            //vBox_insideleft_left.Add(label_insideleft_left1);
            vBox_insideleft_left.Add(text_insideleft_left1);
            vBox_insideleft_left.Add(label_insideleft_left2);
            vBox_insideleft_left.Add(label_insideleft_left3);
            vBox_insideleft_left.Add(label_insideleft_left4);
            vBox_insideleft_right.Add(label_insideleft_right0);
            vBox_insideleft_right.Add(label_insideleft_right1);
            vBox_insideleft_right.Add(label_insideleft_right2);
            vBox_insideleft_right.Add(label_insideleft_right3);
            vBox_insideleft_right.Add(label_insideleft_right4);

            //事件处理
            var bbox_right4 = new EventBox();
            var btn_right4 = new Button($"OK");
            btn_right4.SetSizeRequest(100, 50);
            bbox_right4.Add(btn_right4);
            bbox_right4.ButtonPressEvent += (s, e) => { label_insideleft_right4.Text = context_text_insideleft_left1.Text; };
            vBox_insideright.PackStart(bbox_right4, false, false, 5);
        }


    }
}
